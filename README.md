## 打包步骤
1. 打包mysql依赖docker镜像

    ```sudo docker build -t xxpay-mysql:1.0.0 ./xxpay-mysql```

2. 编译打包java项目依赖

    ```mvn clean package -DskipTests```

3. 打包java各模块docker镜像

    ```sudo mvn docker:build```

4. 启动所有服务依赖

    ```sudo docker-compose up -d```
